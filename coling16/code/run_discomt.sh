#!/bin/bash

# Exit immediately if a command exits with a non-zero status.
set -e

#export PYTHONPATH=$PYTHONPATH:path_to/cnn/pycnn/ #Export pycnn in your python path if needed

SRC=$1 #Path to the directory containing discomt.py path_to/coling16/code/src/
DATA=$2 #Path to the data directory e.g. path_to/coling16/data/
OUT=$3 #Path to the output directory
PY=$4 #Path to your python if needed

export PYTHONPATH=$PYTHONPATH:/home/bplank/tools/cnn/pycnn/
# SRC=discoursecph/multitask/code-v2/
# DATA=discoursecph/multitask/data/
# OUT=temp_pred_disco_mt/
# PY=/home/bplank/anaconda/envs/p3k/bin/


mkdir -p ${OUT}

# Parameters used
SEED=3068836234
INDIM=64
MEM=1024
SIGMA=0.2
HLAYERS=2
HDIM=200
ITER=2

# Data sets for main task, aux views and aux tasks
TEST=${DATA}/main-task/test/ # Dev/Test directory for the main task
TRAINRST=${DATA}/main-task/train/ # Train directory for the main task

TRAINNUC=${DATA}/aux-views/const-nuc/train/ # Train aux view Nuc
TRAINLABEL=${DATA}/aux-views/const-rel/train/ # Train aux view Rel
TRAINDEP=${DATA}/aux-views/dep/train/ # Train aux view Dep
RSTFINE=${DATA}/aux-views/const-fine/train/ # Train aux view fine-grained

TRAINPDTB=${DATA}/aux-tasks/pdtb_inter/ # PDTB
ASPECT=${DATA}/aux-tasks/aspect_majority/ # Aspect
FACT=${DATA}/aux-tasks/factuality_majority/ # Factuality
MOD=${DATA}/aux-tasks/modality_majority/ # Modality
TENSE=${DATA}/aux-tasks/tense_majority/ # Tense
POL=${DATA}/aux-tasks/polarity_majority/ # Polarity
COREF=${DATA}/aux-tasks/coref-ontonotes/ # Coreference

# Pre-trained embeddings
EMBEDS=wd_embeddings/en.polyglot.txt


# Baseline system
TASK=rstc
echo OUT task:${TASK} ite:${ITER} lay:${HLAYERS} hdim:${HDIM} sigma:${SIGMA}

${PY}/python ${SRC}/discomt.py --cnn-mem ${MEM} --cnn-seed ${SEED} --in_dim ${INDIM} --iters ${ITER} --h_dim ${HDIM} --sigma ${SIGMA} --embeds ${EMBEDS} --h_layers ${HLAYERS} --pred_layer 2 --train ${TRAINRST} --test ${TEST} --output ${OUT}/preds-disco.task-${TASK}.ite-${ITER}.lay-${HLAYERS}.hdim-${HDIM}.sigma-${SIGMA} --save ${OUT}/model-disco.task-${TASK}.ite-${ITER}.lay-${HLAYERS}.hdim-${HDIM}.sigma-${SIGMA}



# Best combination (all tasks are predicted on the outer most layer)
TASK=rstc-nuc-lab-rstd-mod-pdtb
echo OUT task:${TASK} ite:${ITER} lay:${HLAYERS} hdim:${HDIM} sigma:${SIGMA}

${PY}/python ${SRC}/discomt.py --cnn-mem ${MEM} --cnn-seed ${SEED} --in_dim ${INDIM} --iters ${ITER} --h_dim ${HDIM} --sigma ${SIGMA} --embeds ${EMBEDS} --h_layers ${HLAYERS} --pred_layer 2 2 2 2 2 2 --train ${TRAINRST} ${TRAINNUC} ${TRAINLABEL} ${TRAINDEP} ${MOD} ${TRAINPDTB} --test ${TEST} --output ${OUT}/preds-disco.task-${TASK}.ite-${ITER}.lay-${HLAYERS}.hdim-${HDIM}.sigma-${SIGMA} --save ${OUT}/model-disco.task-${TASK}.ite-${ITER}.lay-${HLAYERS}.hdim-${HDIM}.sigma-${SIGMA}


for TASK in rstc rstc-nuc-lab-rstd-mod-pdtb
do
    python ${SRC}/parser_scorer.py --test ${TEST} --cpred ${OUT}/preds-disco.task-${TASK}.ite-${ITER}.lay-${HLAYERS}.hdim-${HDIM}.sigma-${SIGMA}
done

