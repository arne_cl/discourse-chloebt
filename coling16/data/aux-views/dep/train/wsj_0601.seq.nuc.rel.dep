In an age of specialization , the federal judiciary is one of the last bastions of the generalist .	root
A judge must jump from murder to antitrust cases , from arson to securities fraud ,	-1_NS-Explanation
without missing a beat .	-1_NS-Manner-Means
But even on the federal bench , specialization is creeping in ,	-3_NN-Contrast
and it has become a subject of sharp controversy on the newest federal appeals court .	-1_NS-Elaboration
The Court of Appeals for the Federal Circuit was created in 1982	-5_NS-Elaboration
to serve , among other things , as the court of last resort for most patent disputes .	-1_NS-Enablement
Previously , patent cases moved through the court system to one of the 12 circuit appeals courts .	-2_NS-Background
There , judges	-1_NS-Elaboration
who saw few such cases	-1_NS-Elaboration
and had no experience in the field	-1_NN-Joint
grappled with some of the most technical and complex disputes imaginable .	-3_NN-Same-unit
A new specialty court was sought by patent experts ,	-4_NN-Topic-Comment
who believed	1_SN-Attribution
that the generalists had botched too many important , multimillion-dollar cases .	-2_NS-Elaboration
Some patent lawyers had hoped	1_SN-Attribution
that such a specialty court would be filled with experts in the field .	-4_NS-Elaboration
But the Reagan administration thought otherwise ,	-1_NN-Contrast
and so may the Bush administration .	-1_NN-Joint
Since 1984 , the president has filled four vacancies in the Federal Circuit court with non-patent lawyers .	-3_NS-Background
Now only three of the 12 judges	-1_NS-Elaboration
-- Pauline Newman , Chief Judge Howard T. Markey , 68 , and Giles Rich , 85 --	-1_NS-Elaboration
have patent-law backgrounds .	-2_NN-Same-unit
The latter two and Judge Daniel M. Friedman , 73 , are approaching senior status or retirement .	-3_NS-Elaboration
Three seats currently are vacant	-5_NS-Elaboration
and three others are likely to be filled within a few years ,	-1_NN-Joint
so patent lawyers and research-based industries are making a new push	-7_NN-Topic-Comment
for specialists to be added to the court .	-1_NS-Elaboration
Several organizations ,	-2_NS-Elaboration
including the Industrial Biotechnical Association and the Pharmaceutical Manufacturers Association ,	-1_NS-Elaboration
have asked the White House and Justice Department to name candidates with both patent and scientific backgrounds .	-2_NN-Same-unit
The associations would like the court to include between three and six judges with specialized training .	-3_NS-Elaboration
Some of the associations have recommended Dr. Alan D. Lourie , 54 , a former patent agent with a doctorate in organic chemistry	-4_NS-Elaboration
who now is associate general counsel with SmithKline Beckman Corp. in Philadelphia .	-1_NS-Elaboration
Dr. Lourie says	1_SN-Attribution
the Justice Department interviewed him last July .	-3_NS-Elaboration
Their effort has received a lukewarm response from the Justice Department .	-4_NS-Elaboration
`` We do not feel	1_SN-Attribution
that seats are reserved	-2_NS-Explanation
( for patent lawyers ) , ''	-1_NS-Elaboration
says Justice spokesman David Runkel ,	-2_NS-Attribution
who declines to say	1_SN-Attribution
how soon a candidate will be named .	-2_NS-Elaboration
`` But we will take it into consideration . ''	-1_NS-Elaboration
The Justice Department 's view is shared by other lawyers and at least one member of the court , Judge H. Robert Mayer , a former civil litigator	-8_NS-Elaboration
who served at the claims court trial level	-1_NS-Elaboration
before he was appointed to the Federal Circuit two years ago .	-2_NS-Temporal
`` I believe	1_SN-Attribution
that any good lawyer should be able to figure out and understand patent law , ''	-4_NS-Explanation
Judge Mayer says ,	-1_NS-Attribution
adding	1_SN-Elaboration
that `` it 's the responsibility of highly paid lawyers	-3_NS-Elaboration
( who argue before the court )	-1_NS-Elaboration
to make us understand	-2_NN-Same-unit
( complex patent litigation ) . ''	-1_NS-Elaboration
Yet some lawyers point to Eli Lilly & Co. vs. Medtronic , Inc. , the patent infringement case the	-19_NN-Contrast
Supreme Court this month agreed to review , as an example of poor legal reasoning by judges	-1_NS-Explanation
who lack patent litigation experience .	-1_NS-Elaboration
( Judge Mayer was not on the three-member panel . )	-3_NS-Elaboration
In the Lilly case , the appeals court broadly construed a federal statute	-4_NS-Elaboration
to grant Medtronic , a medical device manufacturer , an exemption	-1_NS-Elaboration
to infringe a patent under certain circumstances .	-1_NS-Enablement
If the Supreme Court holds in Medtronic 's favor ,	1_SN-Condition
the decision will have billion-dollar consequences for the manufacturers of medical devices , color and food additives and all other non-drug products	-4_NS-Elaboration
that required Food & Drug Administration approval .	-1_NS-Elaboration
Lisa Raines , a lawyer and director of government relations for the Industrial Biotechnical Association , contends	1_SN-Attribution
that a judge well-versed in patent law and the concerns of research-based industries would have ruled otherwise .	-11_NS-Elaboration
And Judge Newman , a former patent lawyer , wrote in her dissent	1_SN-Attribution
when the court denied a motion for a rehearing of the case by the full court ,	-2_NS-Elaboration
`` The panel 's judicial legislation has affected an important high-technological industry ,	-1_NN-Cause
without regard to the consequences for research and innovation or the public interest . ''	-1_NS-Manner-Means
Says Ms. Raines ,	1_SN-Attribution
`` ( The judgment )	-6_NS-Elaboration
confirms our concern	-1_NN-Same-unit
that the absence of patent lawyers on the court could prove troublesome . ''	-1_NS-Elaboration
