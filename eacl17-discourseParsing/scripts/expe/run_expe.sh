

SRC=eacl17_code/
# discoursecph/multilingual/expe/en-rstdt/
expedir=$1
# discoursecph/multilingual/data/en-rtdt/
data=$2
# name of the directory where the models are kept
#modelname=$3
# template file
tpls=$3
# score file
scores=$4
# give a train file (to be able to make expe using another train set than the one in data)
train=$5
#embed=$6
dims=$6

# mkdir exp24aout
# out files
# models=${expedir}/models_en_s0s1q0_2H_020916/
models=${expedir}
mkdir -p ${models}

### Data Not used
# embeddings=${data}/embeddings_edus_svd-50.txt

# tbk files
#train=${data}/train.tbk
dev=${data}/dev.tbk

# raw files
rawdev=${data}/dev.raw
rawtest=${data}/test.raw

# reference files
devref=${data}/constituent_format/dev.dmrg
testref=${data}/constituent_format/test.dmrg


### parameters
it=20       # iterations
lr=0.01     # learning_rate
dc=1e-7     # decrease constant

threads=18

#
for lr in 0.01 #0.02 0.03
do 
for dc in 1e-5 #1e-6 1e-7 0
do
for h in 128 #256
do
    ((j=j%threads)); ((j++==0)) && wait
    (

    # model name
    model=${models}/tpls_2H_h${h}_lr${lr}_dc${dc}
    mkdir ${model}
    
    # training command line, log printed in model/trainer_log.txt
    echo ./${SRC}/hyparse_fork/build/nnt -i ${it} -t ${tpls} -m ${model} -l ${lr} -d ${dc} -H ${h} -H ${h} -K ${dims} -a -f 2 ${train} ${dev} # -L ${embed}
    ./${SRC}/hyparse_fork/build/nnt -i ${it} -t ${tpls} -m ${model} -l ${lr} -d ${dc} -H ${h} -H ${h} -K ${dims} -a -f 2 ${train} ${dev} 2> ${model}/trainer_log.txt #-L ${embed}

    # for each model (i.e. the model dumped at each iteration
    for i in `seq 1 ${it}`
    do 
        # get missing pieces
        for f in encoder ttd embed_dims templates
        do
            cat ${model}/${f} > ${model}/iteration${i}/${f}
        done
        
        # parse with beam = 32 16 8 4 2 1 (larger beam is possible)
        for b in 1 #32 16 8 4 2 1
        do
            # modifies the model/beam file to choose a custom beamsize
            echo ${b} > ${model}/iteration${i}/beam
            # parse dev and test
            ./${SRC}/hyparse_fork/build/nnp -I ${rawdev} -O ${model}/dev_it${i}_beam${b} -m ${model}/iteration${i}
            ./${SRC}/hyparse_fork/build/nnp -I ${rawtest} -O ${model}/test_it${i}_beam${b} -m ${model}/iteration${i}
            
            # evaluate dev and test output
            python ${SRC}/scripts/eval_parser.py --preds ${model}/dev_it${i}_beam${b} --gold ${devref} --params set=dev_lr=${lr}_dc=${dc}_h=${h}_it=${i}_beam=${b} >>${scores}
            #> ${model}/eval_dev_it${i}_beam${b}
            python ${SRC}/scripts/eval_parser.py --preds ${model}/test_it${i}_beam${b} --gold ${testref} --params set=test_lr=${lr}_dc=${dc}_h=${h}_it=${i}_beam=${b} >>${scores}
            #> ${model}/eval_test_it${i}_beam${b}


        done
    done
    
    ) &
done
done
done

#Cocophotos: should wait for all jobs to finish before finishing itself
for job in $(jobs -p); do
    wait $job;
done




