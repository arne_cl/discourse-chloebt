#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  tree_converter.py
#  
#  2014 Sarah Beniamine <sarah.beniamine@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import fastptbparser as Parser
import LabelledTree as Tree
import argparse

def read_action(action,root):
    add_function = lambda mytree : Tree.add_dummy_root(mytree,dummyroot=root)
    remove_function = lambda mytree : Tree.remove_dummy_root(mytree,dummyroot=root)
    replace_function = lambda mytree : Tree.add_dummy_root(mytree.first_child(),dummyroot=root)
    write_function = lambda mytree : mytree

    action_dict = {
        "add": (add_function,        "ajout de la racine \""+root+"\""),
        "remove":(remove_function,   "suppression de la raçine \""+root+"\""),
        "replace":(replace_function, "Remplacement de la raçine par \""+root+"\""),
        "write":(write_function,     "Réécriture des arbres")}
        
    return action_dict[action]
    
def command_line_parser():
    parser = argparse.ArgumentParser(description=main.__doc__)
    
    parser.add_argument("-i", "--input", nargs="?", action="store", type=str, default ="", help = u"Fichier d'entrée")
    parser.add_argument("-o", "--output", nargs="?", action="store", type=str, default ="tree_converter_output.txt", help = u"Fichier de sortie (défaut : tree_converter_output.txt)")
    parser.add_argument("-d", "--dummy_root", nargs="?", action="store", type=str, default ="", help = u"Raçine factice à ajouter/supprimer (défaut: vide)")
    parser.add_argument("-f", "--failure_code", nargs="?", action="store", type=str, default ="(())", help = u"Code à utiliser pour coder l'échec d'analyse (défaut : (()) )")
    
    parser.add_argument("action", choices=['add', 'replace', 'remove','write'], action="store", type=str, help = u"Actions")

    return parser.parse_args()
    
def rewrite_trees(action_text,failure,transform,instream,outstream):

    print action_text, "\nCode d'erreur: ",failure 
    cpt = 1
    
    for mytree in Parser.parse_treebank(instream):
        
        newtree = mytree if mytree.is_parse_failure() else transform(mytree)
        outstream.write(newtree.do_flat_string(failure_code=failure)+"\n")
        print "\r"+str(cpt)+" arbre(s) transformé(s)",
        cpt += 1
        
    instream.close()
    outstream.close()
    
def main():
    
    args = command_line_parser()

    print args

    filename     = args.input
    output      = args.output
    root = args.dummy_root
    failure = args.failure_code
    transform,action_text = read_action(args.action,root)
    
   ## IO
    try:
        instream = open(filename,'r')
    except IOError:
        print "Le fichier d'entrée est obligatoire ! (option -i <filename>)"
        raise
    outstream = open(output,'w')


    ## Transformation
    rewrite_trees(action_text,failure,transform,instream,outstream)


if __name__ == '__main__':
   
	main()
