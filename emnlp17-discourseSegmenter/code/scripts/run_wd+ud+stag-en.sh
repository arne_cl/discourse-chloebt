#!/bin/bash

# Exit immediately if a command exits with a non-zero status.
#set -e


SRC=$1
OUT=$2
mkdir -p ${OUT}

CODE=${SRC}/code/
DATA=${SRC}/data/en/

WD=${DATA}word/
POSU=${DATA}posud/
HDIR=${DATA}hdir/
HLAB=${DATA}hlab/
HPOS=${DATA}hpos/

HPOSHDIR=${DATA}hposdir/

HHLAB=${DATA}hhlab/
HHTOK=${DATA}hhtok/
HTOK=${DATA}htok/

SLEFT=${DATA}sleft/
SRIGHT=${DATA}sright/

MAXITER=10
HLAY=1


for INDIMw in 300 #50 100 20
do
    for INDIMp in 64 
    do
        for HDIM in 200 #400
        do

            IND=32

        PRED=${OUT}preds_doc-words+posud+newstagnoleftright-en-id${INDIMw}_${INDIMp}_${IND}-hd${HDIM}-hl${HLAY}/
        mkdir -p ${PRED}

        INDIM=(${INDIMw} ${INDIMp} ${IND} ${IND} ${IND} ${INDIMw} ${INDIMw})

        echo DOC WORDS+POSUD+NSTAG ITER ${MAXITER} INDIM ${INDIM[*]} HDIM ${HDIM} HLAYERS ${HLAY}

        
        python ${CODE}segmenter.py --train ${WD}train/ ${POSU}train/ ${HLAB}train/ ${HPOSHDIR}train/ ${HHLAB}train/ ${HHTOK}train/ ${HTOK}train/ --dev ${WD}dev/ ${POSU}dev/ ${HLAB}dev/ ${HPOSHDIR}dev/ ${HHLAB}dev/ ${HHTOK}dev/ ${HTOK}dev/ --test ${WD}test/ ${POSU}test/ ${HLAB}test/ ${HPOSHDIR}test/  ${HHLAB}test/ ${HHTOK}test/ ${HTOK}test/ --in_dim ${INDIM[*]} --h_dim ${HDIM} --h_layers ${HLAY} --max_iter ${MAXITER} --outpath ${PRED}

        done
    done
done

